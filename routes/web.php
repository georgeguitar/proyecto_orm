<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\User;
use App\Models\Post;
use App\Models\Publish;
use App\Models\Language;

Route::get('/', function () {
    return view('welcome');
});

// Para los usuarios
Route::get('usuarios', function () {
    return User::get();
});

// Sacar a un usuario de acuerdo a un id
Route::get('usuarios/{usuario_id}', function ($usuario_id) {
    return User::find($usuario_id);
});

// Buscar a un usuario por el correo electronico
Route::get('usuarios/correo/{usuario_correo}', function ($usuario_correo) {
    return User::where("email", "=", $usuario_correo)->first();
});


Route::get('post_user/correo/{correoUs}', function ($correoUs) {
    $Posteos=Post::join("users","users.id","=","autor_id")
    ->where('users.email','=',$correoUs)
    ->get();
    return $Posteos;
});

Route::get('Cantidadpost_user/correo/{correoUs}', function ($correoUs) {
    $Posteos=Post::join("users","users.id","=","autor_id")
    ->where('users.email','=',$correoUs)
    ->get()->count();
    return $Posteos;
});
Route::get('publicados/cantidad', function () {
    $publicados = Publish::join("posts","posts.autor_id","=","publishes.id")
    ->where('publishes.is_publish','=', 1)
    ->get()->count();
    return $publicados;
});

Route::get('lenguaje/post/{idioma}', function ($idioma) {
    $lenguajePost = Language::join("language_post","language_post.language_id","=","languages.id")
    ->where('languages.id','=', $idioma)
    ->get()->count();
    return $lenguajePost;
});
