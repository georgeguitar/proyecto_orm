## CONTRIBUTING

El proyecto desarrollado es un blog basado en la aplicacion del ORM Eloquent de Laravel, a traves de un diagrama entidad relacion que consta de 10 tablas relacionadas entre si.

Em Gitlab se trabajo de la siguiente forma:
## Manejo de branches
  Tenemos 2 branches principales y 2 branches por featureNombre
  - Developing - Es el branch de desarrollo se harán pull requests cada que una tarea se termine
  - Master - Es el branch del que se realizará el deploy a producción
  - featureNombre - Es el branch por integrante

## Flujo de trabajo
  * Se Creará un branch con las reglas especificadas
    * git checkout develop
    * git pull
    * git checkout -b featureNombre
      * Por ejemplo 
         * featureClaudia
  
  * Crea y modifica archivos necesarios de acuerdo al template
    * git add .
    * git commit -m "Mensaje descriptivo"
    * git fetch
    * git push -u origin featureNombre
  * Una vez que termine las tareas se debe realizar un merge request al branch develop mediante la página de gitlab
  * Un revisor (cualquier developer) revisará el trabajo~ 
	* Si el revisor encuentra algún problema devuelve el issue al developer original indicando el problema en los comentarios del merge request
	* Si el revisor considera que el grupo o equipo cumple el formato:
	  * Aprueba el merge request	  
  
  * Al final se hará un merge request de developer a master

## Herramientas a Utilizar

* GitLab
    Un sistema de gestión  de código e incidencias, es, al mismo tiempo, muy parecido y muy diferente a otros como Github, o Bitbucket.
    Enlace del proyecto Movie.info:
    Url=  https://gitlab.com/georgeguitar/proyecto_orm.git

* Hangouts
    Una herramienta que permite al equipo realizar video conferencias, y mantener reuniones on line. 

* Whatsapp
    Una herramienta que permite tener en contacto a todo el equipo y coordinar las diferentes reuniones, o preguntas entre los integrantes sobre el proyecto.

## Team

   - El equipo decidio utilizar todas las herramientas mencionadas, para poder tener una mejor organizacion del proyecto.
   - Se establecio un horario de trabajo, 10:00 pm, donde cada integrante ingresaba a hangouts para coordinar lo que habia revisado,
     investigado y realizar la programacion correspondiente.
   - En Gitlab se realizo el trabajo en Ramas .
   - Durante la realizacion del proyecto nos colaboramos entre todos para lograr un mejor proyecto