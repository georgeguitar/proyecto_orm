<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    
    protected $fillable = [
        'first_name',
        "last_name",
        "email",
        "password",
        "remember_token"
    ];
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
        "id"
    ];
    
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
    
    public function publishes()
    {
        return $this->belongsToMany(User::class);
    }
}
