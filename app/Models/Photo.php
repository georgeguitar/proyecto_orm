<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $table = "photos";
    
    protected $fillable = [
        'filename',
        "type"
    ];
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];
    
    public function posts()
    {
        return $this->belongsToMany(Post::class)->withPivot('use', 'order')->withTimestamps();
    }
}
