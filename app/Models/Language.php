<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = "languages";
    
    protected $fillable = [
        'label',
        "iso6391"
    ];
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];
    
    public function categories()
    {
        return $this->belongsToMany(Category::class)->withPivot('label', 'slug', 'description');
    }
    
    public function posts()
    {
        // Añadiendo los pivots 
        return $this->belongsToMany(Post::class)->withPivot('title', 'slug', 'content');
    }
}
