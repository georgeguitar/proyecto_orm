<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    
    protected $fillable = [
        'comment'
    ];
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id",
        "posts_id"
    ];

}
