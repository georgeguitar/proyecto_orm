<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publish extends Model
{
    protected $table = "publishes";
    
    protected $fillable = [
        'slug',
        "label",
        "is_publish"
    ];
    
    protected $casts =  [
        "is_publish"  => "boolean",
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];
    
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
