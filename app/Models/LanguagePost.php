<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguagePost extends Model
{
    public $timestamps = false;
    protected $table = "language_post";
    protected $primaryKey = ['post_id', 'language_id'];
    public $incrementing = false;
    
    protected $fillable = [
        'title',
        "slug",
        "content"
    ];
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];
}
