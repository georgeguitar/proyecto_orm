<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoPost extends Model
{
    protected $table = "photo_post";
    protected $primaryKey = ['photo_id', 'post_id'];
    public $incrementing = false;
    
    protected $fillable = [
        'use',
        "order"
    ];
    
    protected $casts =  [
        "order"  => "integer",
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id",
        "photo_id",
        "post_id"
    ];
}
