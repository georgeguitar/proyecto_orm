<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    protected $fillable = [
        'type',
        "age"
    ];
    
    protected $casts =  [
        "age"  => "integer",
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];
    
    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
    
    public function languages()
    {
        // Añadiendo los pivots
        return $this->belongsToMany(Language::class)->withPivot('title', 'slug', 'content');
    }
    
    public function photos()
    {
        return $this->belongsToMany(Photo::class)->withPivot('use', 'order')->withTimestamps();
    }
    
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
