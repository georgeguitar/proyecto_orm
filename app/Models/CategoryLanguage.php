<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryLanguage extends Model
{
    public $timestamps = false;
    protected $table = "category_language";
    protected $primaryKey = ['category_id', 'language_id'];
    public $incrementing = false;
    
    protected $fillable = [
        'label',
        'slug',
        'description'
    ];
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
    ];
    
}

