<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    
    protected $casts =  [
        'updated_at'    => "date",
        'created_at'    => "date"
    ];
    
    protected $hidden = [
        "created_at",
        "updated_at",
        "id"
    ];
    
    public function languages()
    {
        return $this->belongsToMany(Language::class)->withPivot('label', 'slug', 'description');
    }
    
}
