# Proyecto para el módulo ORM

## Mapeo Objeto Relacional

El requerimiento es hacer un Mapeo Objeto Relacional utilizando Laravel y su ORM Elocuent.
Se tendrá:
1. Repositorio en Gitlab o Github.
2. Utilizar MySQL como motor de Base de Datos.
3. Simulacion de un blog.
4. Estara basado en el modelo entidad relacion proporcionado.

## Integrantes

Navarro Arias Juan Dirceu.  
Navarro Arias Luis Fernando Numa.  
Salazar Gonzales Claudia.  

## Herramientas

Whatsapp.  
Hangouts.  
GitLab.  

## Proceso
Se especifica en el archivo "Contributing".