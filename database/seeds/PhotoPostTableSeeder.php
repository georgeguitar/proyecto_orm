<?php

use Illuminate\Database\Seeder;

class PhotoPostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         factory(App\Models\PhotoPost::class, 50)->create();
     }
    /*
    public function run()
    {
        $posts = App\Models\Post::get();
        
        App\Models\Photo::all()->each(function ($photos) use ($posts) {
            $posts_random = $posts->random( rand(1,10) );
            $photos->posts()->sync($posts_random);
        });
    }
    */
}
