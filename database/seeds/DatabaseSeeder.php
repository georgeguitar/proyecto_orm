<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(PublishesTableSeeder::class);
        $this->call(PhotosTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        
        $this->call(PostsTableSeeder::class);
        $this->call(PhotoPostTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        
        $this->call(CategoryLanguagesTableSeeder::class);
        $this->call(LanguagePostTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
    }
}
