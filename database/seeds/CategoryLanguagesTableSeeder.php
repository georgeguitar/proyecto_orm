<?php

use Illuminate\Database\Seeder;

use App\Models\Category;
use App\Models\Language;

class CategoryLanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        factory(App\Models\CategoryLanguage::class, 50)->create();
    }
}
