<?php

use Illuminate\Database\Seeder;

class LanguagePostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         factory(App\Models\LanguagePost::class, 50)->create();
     }

/*    
    public function run()
    {
        $languages = App\Models\Language::get();
        
        App\Models\Post::all()->each(function ($post) use ($languages) {
            $languages_random = $languages->random( rand(1,10) );
            $post->languages()->sync($languages_random);
        });
    }
*/
}

