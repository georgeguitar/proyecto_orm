<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_post', function (Blueprint $table) {
            $table->string('use')->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
            $table->integer('photo_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->foreign('photo_id')->references('id')->on('photos');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->primary(['photo_id', 'post_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_post');
    }
}
