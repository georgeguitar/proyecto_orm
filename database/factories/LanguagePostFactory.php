<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });

$factory->define(App\Models\LanguagePost::class, function (Faker $faker) {
	// Se ha sacado los id de las tablas
	$postIds = App\Models\Post::get()->pluck('id')->toarray();	
	$languageIds = App\Models\Language::get()->pluck('id')->toArray();

    return [
    	// Añadiendo los random 
    	'post_id' => $faker->unique()->randomElement($postIds),
    	'language_id' => $faker->randomElement($languageIds),
    	// Se mantiene
        'title' => $faker->realText($maxNbChars = 200, $indexSize = 2),
//        'slug' => str_slug($faker->realText($maxNbChars = 200, $indexSize = 2)),
        'slug' => str_slug($faker->realText($maxNbChars = 20, $indexSize = 2)),
        'content' => $faker->text,
    ];
});