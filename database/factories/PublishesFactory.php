<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });


$factory->define(App\Models\Publish::class, function (Faker $faker) {
    return [
        'slug' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'label' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'is_publish' => rand(0, 1),
    ];
});