<?php

use Faker\Generator as Faker;

$factory->define(App\Models\CategoryLanguage::class, function (Faker $faker) {
	// Se ha sacado los id de las tablas
//	$categoryIdsCL = App\Models\Category::get()->pluck('id')->toarray();	
//	$languageIdsCL = App\Models\Language::get()->pluck('id')->toArray();

    return [
//    	'category_id' => $faker->unique()->randomElement($categoryIdsCL),
//    	'language_id' => $faker->randomElement($languageIdsCL),

        'category_id' => $faker->numberBetween(1, 50),
        'language_id' => $faker->unique()->numberBetween(1, 50),        
        'label' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'slug' => str_slug($faker->realText($maxNbChars = 20, $indexSize = 2)),
//        'slug' => str_slug($faker->realText($maxNbChars = 200, $indexSize = 2)),
        'description' => $faker->text,
    ];
});