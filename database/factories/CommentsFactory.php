<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'autor_id' => rand(1, 10),
        'comment' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'post_id' => rand(1, 10),
    ];
});