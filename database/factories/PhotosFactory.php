<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });

$factory->define(App\Models\Photo::class, function (Faker $faker) {
    return [
    //         'filename' => file($sourceDir = '/tmp', $targetDir = '/tmp'),
        'filename' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'type' => $faker->realText($maxNbChars = 200, $indexSize = 2),
    ];
});
