<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });


$factory->define(App\Models\Language::class, function (Faker $faker) {
    return [
        'label' => $faker->text,
        'iso6391' => str_random(2),
    ];
});