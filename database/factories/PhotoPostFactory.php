<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });

$factory->define(App\Models\PhotoPost::class, function (Faker $faker) {
	// Se ha sacado los id de las tablas
	$postIds = App\Models\Post::get()->pluck('id')->toarray();	
	$photoIds = App\Models\Photo::get()->pluck('id')->toArray();


    return [
        'use' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'order' => rand(1, 10),
        'photo_id' => $faker->randomElement($photoIds),
    	'post_id' => $faker->randomElement($postIds),
        /*
        'photo_id' => rand(1, 10),
        'post_id' => rand(1, 10),
        */
    ];
});
