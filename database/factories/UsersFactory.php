<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });


$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'fist_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(100),
        'deleted_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = '+30 days'),
    ];
});