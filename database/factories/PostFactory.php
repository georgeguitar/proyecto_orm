<?php

use Faker\Generator as Faker;

// $factory->define(Model::class, function (Faker $faker) {
//     return [
//         //
//     ];
// });


$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'autor_id' => rand(1, 10),
        'publishes_id' => rand(1, 10),
        'publish_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = '+30 days'),
        'deleted_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = '+30 days'),
    ];
});